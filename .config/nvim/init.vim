""" PLUGINS
if has("nvim")
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
else
  if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif

set hidden
set nocompatible

call plug#begin('~/.vim/plugged')
Plug 'sheerun/vim-polyglot'
Plug 'w0rp/ale'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-repeat'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'lifepillar/vim-solarized8'
call plug#end()



""" VIM CORE SETTINGS
syntax enable
set mouse=a
set ignorecase
set smartcase
set incsearch
set showmatch
set relativenumber
set number
set smartindent
set backspace=start,eol,indent
set laststatus=2
set clipboard=unnamed
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set fillchars=vert:\ ,stl:\ ,stlnc:\ 
set timeoutlen=500
set inccommand=nosplit
set foldlevelstart=20

" RELOCATE SWP/TMP FILES
silent !mkdir ~/.vim-tmp > /dev/null 2>&1
set backup
set backupdir=~/.vim-tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp
set writebackup

" NETRW
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 15
let g:netrw_keepdir = 0

""" BINDINGS
let mapleader = "\<Space>"
" FZF
nnoremap <silent><leader><space><space> :GFiles<CR>
nnoremap <silent><leader><space> :GFiles<CR>
nnoremap <silent><leader><space>b :Buffers<CR>
nnoremap <silent><leader><space>f :Rg<CR>
nnoremap <silent><leader><space>g :BCommits<CR>
nnoremap <silent><leader><space>h :Helptags<CR>
nnoremap <silent><leader><space>r :Files<CR>
" UTILITIES
nnoremap <silent><leader><backspace><backspace> :e <c-r>=$MYVIMRC<CR><CR>
nnoremap <silent><leader><backspace> :source <c-r>=$MYVIMRC<CR><CR>
nnoremap <silent><leader><tab> :Lexplore <c-r>=getcwd()<CR><CR>
nnoremap <silent><leader><esc> :on<CR>
nnoremap <silent><leader>] :lnext<CR>
nnoremap <silent><leader>[ :lprevious<CR>
nnoremap <silent><leader>} :cnext<CR>
nnoremap <silent><leader>{ :cprevious<CR>
nnoremap <silent><leader>\ :cwindow<CR>
nnoremap <silent><leader>\\ :lclose<CR>
nnoremap <silent><leader>bg :let &background = ( &background == "dark"? "light" : "dark" )<CR>
nnoremap <silent><leader>/ :noh<CR>
" ALE
nnoremap <silent><leader>d :ALEGoToDefinition<CR>
nnoremap <silent><leader>o :ALEHover<CR>
nnoremap <silent><leader>s :ALESymbolSearch<CR>
nnoremap <silent><leader>u :ALEFindReferences<CR>
" BUFFERS
nnoremap <silent><leader>l :bnext<CR>
nnoremap <silent><leader>h :bprevious<CR>
nnoremap <silent><leader>q :bdelete<CR>
nnoremap <silent><leader>v :vert belowright sbnext<CR>
" SPLITS
nnoremap <silent><leader>- :vertical resize -10<CR>
nnoremap <silent><leader>= :vertical resize +10<CR>
" TERM
nnoremap <silent><leader>tt :botright vs term://tmux new -A -s <c-r>=system('base64', getcwd())<CR><CR>



""" AIRLINE
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1



""" ALE
let g:ale_linters = {
\ 'gitcommit': ['proselint', 'writegood', 'gitlint'],
\ 'haskell': ['hie'],
\ 'javascript': ['tsserver', 'tslint'],
\ 'markdown': ['proselint', 'writegood'],
\ 'php': ['langserver', 'php', 'phpcs', 'phpmd'],
\ 'python': ['pyls', 'mypy'],
\ 'rust': ['rls'],
\ 'sh': ['shell', 'shellcheck'],
\ 'text': ['proselint', 'writegood'],
\ 'typescript': ['tsserver', 'tslint'],
\ 'vim': ['vint'],
\ 'zsh': ['shell', 'shellcheck'],
\ }

let g:ale_fixers = {
\ 'css': ['prettier'],
\ 'haskell': ['hfmt'],
\ 'graphql': ['prettier'],
\ 'javascript': ['prettier'],
\ 'php': ['phpcbf'],
\ 'rust': ['rustfmt'],
\ 'json': ['prettier'],
\ 'python': ['yapf', 'isort'],
\ 'scss': ['prettier'],
\ 'typescript': ['tslint', 'prettier'],
\ 'yaml': ['prettier'],
\ }

let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1
let g:ale_python_mypy_options = '--ignore-missing-imports'
let g:ale_python_mypy_ignore_invalid_syntax = 1
let g:ale_haskell_hie_executable = 'hie-wrapper'
let g:ale_set_loclist = 1
let g:ale_set_quickfix = 0



""" LANGUAGES
autocmd Filetype gitcommit setlocal et ts=4 sw=4 sts=4 fdm=manual tw=80
autocmd Filetype markdown setlocal et ts=4 sw=4 sts=4 fdm=manual
autocmd Filetype text setlocal noet ts=4 sw=4 sts=4 fdm=manual

autocmd Filetype haskell setlocal et ts=2 sw=2 sts=2 fdm=indent
autocmd Filetype python setlocal et ts=4 sw=4 sts=4 fdm=indent tw=120

autocmd Filetype javascript setlocal et ts=2 sw=2 sts=2 fdm=syntax
autocmd Filetype typescript setlocal et ts=2 sw=2 sts=2 fdm=syntax
autocmd Filetype graphql setlocal et ts=2 sw=2 sts=2 fdm=syntax
autocmd Filetype proto setlocal et ts=2 sw=2 sts=2 fdm=indent

autocmd Filetype php setlocal et ts=4 sw=4 sts=4 fdm=syntax
autocmd Filetype rust setlocal et ts=4 sw=4 sts=4 fdm=syntax

autocmd Filetype vim setlocal noet ts=2 sw=2 sts=2 fdm=indent
autocmd Filetype sh setlocal noet ts=2 sw=2 sts=2 fdm=indent
autocmd Filetype zsh setlocal noet ts=2 sw=2 sts=2 fdm=indent

autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
au BufRead /tmp/mutt-* set tw=72


""" HACKS
com! FormatXML :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"



""" COLORS
set termguicolors
set background=dark
let g:airline_theme='minimalist'
let g:gruvbox_contrast_dark='medium'
let g:gruvbox_contrast_light='medium'

if &diff
	colorscheme slate
else
	colorscheme gruvbox
endif

