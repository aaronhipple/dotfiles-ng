#!/bin/zsh

# paths and such
export ZSH=$HOME/.oh-my-zsh
export GOPATH="$HOME/go"
export PATH="$VOLDA_HOME/bin:/usr/local/opt/openjdk@11/bin:/usr/local/opt/curl/bin:$HOME/.emacs.d/bin:$HOME/.local/bin:$HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:$HOME/.scripts:$HOME/.gem/ruby/2.5.0/bin:$HOME/Library/Python/3.8/bin"
[ -z "$DISPLAY" ] && [ -f "$HOME/.profile" ] && . "$HOME/.profile"

[ ! -d "$ZSH" ] && git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

# zsh config
ZSH_THEME="theunraveler"
HIST_STAMPS="yyyy-mm-dd"
plugins=(archlinux docker git z sudo)
source $ZSH/oh-my-zsh.sh
 
[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ] && source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] && source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# general environment thangs
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR="$(which nvim)"

if [ -n "$DESKTOP_SESSION" ];then
  export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/gcr/ssh
fi

# keybinding stuff
autoload -U select-word-style
select-word-style bash

if [[ $TERM = *xterm* ]] ; then
  bindkey "\e[3~" delete-char
  bindkey "\e[1;5D" backward-word
  bindkey "\e[1;5C" forward-word
  bindkey "\eOH" beginning-of-line
  bindkey "\eOF" end-of-line

  bindkey "\e[Z" reverse-menu-complete # Shift+Tab
fi

# Grab everything from the .shell.d directory
for f in ~/.shell.d/*.sh; do source $f; done;

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh

case "$(hostname 2>/dev/null || hostnamectl --static)" in
	MacBook-Pro.local)
		export JAVA_HOME='/usr/local/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home'
		;;
esac

[ -f $HOME/.zshrc_ironclad ] && . $HOME/.zshrc_ironclad

source ~/.cargo/env
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
