#!/usr/bin/env zsh

(command -v nvim &> /dev/null) && alias vim=nvim && alias vimdiff='nvim -d'
(command -v hub &> /dev/null) && alias git=hub

export TIME_STYLE="iso"

(unalias ls &> /dev/null) || true

if command -v exa &> /dev/null; then
	CMD="exa --git"
else
	CMD="ls -v"
fi

if [ "$OSTYPE" = "linux-gnu" ]; then
	alias ls="$CMD --classify --color=always --group-directories-first" # nice colors and stuff
else
	alias ls="$CMD --classify" # or...
fi

alias grep='grep --color=auto' # nice colors
alias mkdir='mkdir -p' # don't let me do anything stupid accidentally
alias rm='rm -i'    # REALLY don't let me do anything stupid accidentally
alias cls='printf "\ec"' # clear out the term

reads() {
	tmux new-session -d 'mutt'
	tmux split-window -h 'canto-curses'
	tmux -2 attach-session -d
}

MODE_PATH="/tmp/$USER.mode"

dark() {
	if command -v darkman &> /dev/null; then
		darkman toggle
		return
	fi

	LAST_MODE="$((<"$MODE_PATH") 2> /dev/null || echo "dark")"
	MODE="$([ "$LAST_MODE" = "dark" ] && echo "light" || echo "dark")"

	for script in $HOME/.local/share/$MODE-mode.d/*; do
		$script &> /dev/null || true
	done

	echo "$MODE" > "$MODE_PATH"
}

# https://news.ycombinator.com/item?id=32107654
alias pf="fzf --preview='bat -f {}' --bind shift-up:preview-page-up,shift-down:preview-page-down"

# https://news.ycombinator.com/item?id=32106770
function fcd() {
	local dir;

	while true; do
		# exit with ^D
		dir="$(cat <(echo '..') <(ls -a1D) | fzf --ansi -1 --height 40% --reverse --no-multi --preview 'pwd' --preview-window=up,1,border-none --no-info)"
		if [[ -z "${dir}" ]]; then
			break
		else
			cd "${dir}"
		fi
	done
}
