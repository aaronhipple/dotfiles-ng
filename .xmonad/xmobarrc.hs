Config { 
   -- appearance
     font        = "-*-tamzen-medium-*-*-*-13-*-*-*-*-*-*-*"
   , bgColor     = "#1f1f1f"
   , fgColor     = "#c0b18b"
   , position    = Top
   , border      = BottomB
   , borderColor = "#8f6840"

   -- layout
   , sepChar =  "%"
   , alignSep = "}{"
   , template = "%StdinReader% }{ %battery% | %KASE% | %date% || %kbd% "

   -- general behavior
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      True    -- show on all desktops
   , overrideRedirect = True    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     False   -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled)

   , commands = 
        -- weather monitor
        [ Run Weather "KASE" [ "--template", "<skyCondition> | <fc=#535c5c><tempF></fc>°F | <fc=#535c5c><rh></fc>% | <fc=#535c5c><pressure></fc>hPa"
                             ] 36000

        -- battery monitor
        , Run Battery        [ "--template" , "Batt: <acstatus>"
                             , "--Low"      , "10"        -- units: %
                             , "--High"     , "80"        -- units: %
                             , "--low"      , "#ac5d2f"
                             , "--normal"   , "#8f6840"
                             , "--high"     , "#647035"

                             , "--" -- battery specific options
                                       -- discharging status
                                       , "-o"	, "<left>% (<timeleft>)"
                                       -- AC "on" status
                                       , "-O"	, "<fc=#d17b49>Charging</fc>"
                                       -- charged status
                                       , "-i"	, "<fc=#647035>Charged</fc>"
                             ] 50

        -- time and date indicator 
        --   (%F = y-m-d date, %a = day of week, %T = h:m:s time)
        , Run Date           "<fc=#af865a>%F (%a) %T</fc>" "date" 10

        -- keyboard layout indicator
        , Run Kbd            [ ("us(colemak)" , "<fc=#6d715e>CK</fc>")
                             , ("us"          , "<fc=#775759>US</fc>")
                             ]

        -- general status
        , Run StdinReader
        ]
   }

