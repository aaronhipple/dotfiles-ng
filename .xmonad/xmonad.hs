import           Data.List
import           Graphics.X11.ExtraTypes.XF86
import           XMonad
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.EwmhDesktops    (fullscreenEventHook)
import           XMonad.Layout.NoBorders      (smartBorders)
import           XMonad.Layout.Spacing        (smartSpacing)
import           XMonad.Util.EZConfig         (additionalKeys)

-- Colors
colorForeground = "#c0b18b"

colorBackground = "#1f1f1f"

colorCursorColor = "#c0b18b"

colorLightBlack = "#4a3637"

colorDarkBlack = "#402e2e"

colorLightRed = "#d17b49"

colorDarkRed = "#ac5d2f"

colorLightGreen = "#7b8748"

colorDarkGreen = "#647035"

colorLightYellow = "#af865a"

colorDarkYellow = "#8f6840"

colorLightBlue = "#535c5c"

colorDarkBlue = "#444b4b"

colorLightMagenta = "#775759"

colorDarkMagenta = "#614445"

colorLightCyan = "#6d715e"

colorDarkCyan = "#585c49"

colorLightWhite = "#c0b18b"

colorDarkWhite = "#978965"

-- command escape helper
quote :: String -> String
quote s = "'" ++ s ++ "'"

-- Customized variables
myBarCmd = "xmobar ~/.xmonad/xmobarrc.hs"

myTerminal = "xterm"

myModMask = mod4Mask

myBorderWidth = 3

myLayoutHook = smartSpacing 5 $ smartBorders (layoutHook def)

myHandleEventHook = fullscreenEventHook

myWorkSpaces = ["web", "code", "more"] ++ map show [4 .. 9]

myNormalColor = colorDarkRed

myFocusColor = colorLightRed

myFont = "-*-tamzen-medium-*-*-*-13-*-*-*-*-*-*-*"

myLauncher =
  unwords
    [ "dmenu_run"
    , "-fn"
    , quote myFont
    , "-nb"
    , quote colorBackground
    , "-nf"
    , quote colorForeground
    , "-sb"
    , quote colorDarkBlack
    , "-sf"
    , quote colorDarkWhite
    ]

myAdditionalKeys =
  [ ((myModMask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock")
  , ((myModMask, xK_p), spawn myLauncher)
  , ((0, xF86XK_AudioLowerVolume), spawn "amixer -q sset Master 5%-")
  , ((0, xF86XK_AudioRaiseVolume), spawn "amixer -q sset Master 5%+")
  , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
  ]

-- Config unification
myConfig =
  def
  { borderWidth = myBorderWidth
  , handleEventHook = myHandleEventHook
  , layoutHook = myLayoutHook
  , modMask = myModMask
  , terminal = myTerminal
  , normalBorderColor = myNormalColor
  , focusedBorderColor = myFocusColor
  , workspaces = myWorkSpaces
  } `additionalKeys`
  myAdditionalKeys

-- Customized pretty printer function for xmonad status output.
myPP =
  XMonad.Hooks.DynamicLog.def
  { ppCurrent = xmobarColor colorLightYellow "" . wrap "[" "]"
  , ppTitle = xmobarColor colorLightGreen "" . shorten 50
  , ppVisible = wrap "(" ")"
  , ppUrgent = xmobarColor colorLightRed colorLightYellow
  }

-- Customized status bar launcher
myBar = statusBar myBarCmd myPP toggleStrutsKey myConfig

-- Launch the thing
main = xmonad =<< myBar

-- Keybinding to toggle the gap for the bar.
toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)
