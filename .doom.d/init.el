;;; init.el -*- lexical-binding: t; -*-

;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

;; NOTE Press 'SPC h d h' (or 'C-h d h' for non-vim users) to access Doom's
;;      documentation. There you'll find a "Module Index" link where you'll find
;;      a comprehensive list of Doom's modules and what flags they support.

;; NOTE Move your cursor over a module's name (or its flags) and press 'K' (or
;;      'C-c c k' for non-vim users) to view its documentation. This works on
;;      flags as well (those symbols that start with a plus).
;;
;;      Alternatively, press 'gd' (or 'C-c c d') on a module to browse its
;;      directory (for easy access to its source code).

(setq ah/environment (cond
                      ((string= (system-name) "MacBook-Pro.local") 'work)
                      ((string= (system-name) "evans") 'home)
                      ((string= (system-name) "green") 'home)
                      (t 'unknown)
                      ))

(defconst IS-HOME     (eq ah/environment 'home))
(defconst IS-WORK     (eq ah/environment 'work))

(doom! :completion
       company
       ivy

       :ui
       doom
       doom-dashboard
       doom-quit
       hl-todo
       ligatures
       minimap
       (modeline :light)
       nav-flash
       ophints
       (popup +defaults)
       treemacs
       vc-gutter
       workspaces
       zen

       :editor
       (evil +everywhere)
       file-templates
       fold
       (format +onsave)
       snippets

       :emacs
       dired
       electric
       undo
       vc

       :checkers
       syntax

       :tools
       ;;debugger
       editorconfig
       (eval +overlay)
       lookup
       lsp
       (magit +forge)
       ;;pdf
       rgb
       taskrunner

       :os
       (:if IS-MAC macos)

       :lang
       data
       emacs-lisp
       (json +lsp)
       (javascript +lsp)
       latex
       (markdown +grip)
       org
       plantuml
       (sh +lsp)
       (web +lsp)
       (purescript +lsp)
       (rust +lsp)
       (yaml +lsp)
       (:if IS-WORK
        (java +meghanada)
        (python +lsp)
        )
       (:if IS-HOME
        (haskell +lsp)
        ledger
        (php +lsp)
        )

       :email
       ;;notmuch
       (mu4e +gmail)

       :app
       calendar
       everywhere
       (rss +org)
       (:if IS-WORK
        (slack)
        )

       :config
       (default +bindings))
