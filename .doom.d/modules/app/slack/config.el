;;; $DOOMDIR/modules/slack/config.el -*- lexical-binding: t; -*-

(use-package! emacs-slack
  :defer t
  :init
  (make-directory "/tmp/emacs-slack-images" t)

  (map! :leader
        (:prefix-map ("o s" . "Slack")
         :desc "start slack"      "s" #'slack-start
         :desc "select im"        "i" #'slack-im-select
         :desc "select channel"   "c" #'slack-channel-select
         :desc "view all threads" "t" #'slack-all-threads
         ))

  :custom
  (slack-buffer-emojify t)
  (slack-prefer-current-team t)
  (slack-display-team-name nil)
  (slack-image-file-directory "/tmp/emacs-slack-images")

  :config

  (map! :localleader
        :map slack-info-mode-map
        "u" 'slack-room-update-messages)

  (map! :localleader
        :map slack-mode-map
        "c" 'slack-buffer-kill
        "u" 'slack-room-update-messages
        "2" 'slack-message-embed-mention
        "3" 'slack-message-embed-channel
        )

  (map! :localleader
        :map slack-mode-map
        (:prefix-map ("r" . "reaction")
         :desc "add reaction"        "a" 'slack-message-add-reaction
         :desc "remove reaction"     "r" 'slack-message-remove-reaction
         :desc "show reaction users" "s" 'slack-message-show-reaction-users
         ))

  (map! :localleader
        :map slack-mode-map
        (:prefix-map ("p" . "pins")
         "l" 'slack-room-pins-list
         "a" 'slack-message-pins-add
         "r" 'slack-message-pins-remove
         ))

  (map! :localleader
        :map slack-mode-map
        (:prefix-map ("m" . "message")
         "m" 'slack-message-write-another-buffer
         "e" 'slack-message-edit
         "d" 'slack-message-delete
         ))

  (map! :localleader
        :map slack-edit-message-mode-map
        "k" 'slack-message-cancel-edit
        "s" 'slack-message-send-from-buffer
        "2" 'slack-message-embed-mention
        "3" 'slack-message-embed-channel)
  )
