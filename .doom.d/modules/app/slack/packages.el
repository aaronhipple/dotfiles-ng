;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/modules/slack/packages.el

(package! ox-slack)

(package! alert)
(package! circe)
(package! emojify)
(package! oauth2)
(package! websocket)
(package! emacs-slack
  :recipe (:host github :repo "yuya373/emacs-slack")
  )
