;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Aaron Hipple"

      user-mail-address (cond
                         (IS-WORK "aaron@ironcladhq.com")
                         (IS-HOME "aaron@hippl.es")
                         (t "aaron@hippl.es")
                         )

      doom-theme 'ah-gruvbox
      doom-font (cond
                 (IS-LINUX (font-spec :family "Victor Mono" :size 12 :weight 'medium :width 'normal))
                 (IS-MAC (font-spec :family "Victor Mono" :size 11 :weight 'medium :width 'normal))
                 (t (font-spec :family "Victor Mono" :size 12 :weight 'medium :width 'normal))
                 )

      doom-big-font (cond
                 (IS-LINUX (font-spec :family "Victor Mono" :size 18 :weight 'medium :width 'normal))
                 (IS-MAC (font-spec :family "Victor Mono" :size 18 :weight 'medium :width 'normal))
                 (t (font-spec :family "Victor Mono" :size 18 :weight 'medium :width 'normal))
                 )

      doom-variable-pitch-font (cond
                 (IS-LINUX (font-spec :family "Victor Mono" :size 11 :weight 'medium :width 'normal :slant 'italic))
                 (IS-MAC (font-spec :family "Victor Mono" :size 11 :weight 'medium :width 'normal :slant 'italic))
                 (t (font-spec :family "Victor Mono" :size 11 :weight 'medium :width 'normal :slant 'italic))
                 )

      display-line-numbers-type 'relative

      company-idle-delay nil

      evil-ex-substitute-global t
      evil-split-window-below t
      evil-vsplit-window-right t

      lsp-ui-doc-enable t
      lsp-ui-doc-position nil

      auth-sources `("~/.authinfo.app")

      )

;;; :gpg calendar
(after! org-gcal
  (setq org-gcal-client-id "383815517264-4mm1sg5fnvt54pjlru0trp7vuomhm0lt.apps.googleusercontent.com")
  (setq org-gcal-client-secret (auth-source-pick-first-password :host org-gcal-client-id)
        org-gcal-fetch-file-alist (cond (IS-HOME '(("ahipple@gmail.com" . "~/org/calendars/ahipple@gmail.com.org")
                                                   ("colette.v.armstrong@gmail.com" . "~/org/calendars/colette.v.armstrong@gmail.com.org")
                                                   ))
                                        (IS-WORK '(("aaron@ironcladhq.com" . "~/org/calendars/aaron@ironcladhq.com.org")
                                                   ))
                                        (t '())
                                        ))
  )

;;; :email mu4e
(add-hook 'message-mode-hook 'auto-fill-mode)
(setq-default fill-column 72)
(setq mu4e-speedbar-support t
      mu4e-view-show-images nil
      mu4e-headers-include-related nil

      mu4e-view-prefer-html nil
      mu4e-view-html-plaintext-ratio-heuristic most-positive-fixnum

      mu4e-compose-format-flowed t
      message-cite-reply-position 'below
      fill-flowed-encode-column fill-column

      mu4e-attachment-dir (expand-file-name "~/")

      smtpmail-smtp-service 587
      send-mail-function    'smtpmail-send-it
      )

(when IS-HOME
  (set-email-account! "ahipple@gmail.com"
                      '((mu4e-sent-folder            . "/ahipple@gmail.com/Sent")
                        (mu4e-drafts-folder          . "/ahipple@gmail.com/Drafts")
                        (mu4e-trash-folder           . "/ahipple@gmail.com/Trash")
                        (mu4e-refile-folder          . "/ahipple@gmail.com/All")
                        (smtpmail-smtp-user          . "ahipple@gmail.com")
                        (smtpmail-smtp-server        . "smtp.gmail.com")
                        (mu4e-sent-messages-behavior . delete)
                        (user-mail-address           . "ahipple@gmail.com")
                        (mu4e-compose-signature      . "Aaron"))
                      nil)

  (set-email-account! "aaron@aaronhipple.com"
                      '((mu4e-sent-folder            . "/aaron@aaronhipple.com/Sent")
                        (mu4e-drafts-folder          . "/aaron@aaronhipple.com/Drafts")
                        (mu4e-trash-folder           . "/aaron@aaronhipple.com/Trash")
                        (mu4e-refile-folder          . "/aaron@aaronhipple.com/Archive")
                        (smtpmail-smtp-user          . "aaron@aaronhipple.com")
                        (smtpmail-smtp-server        . "smtp.fastmail.com")
                        (mu4e-sent-messages-behavior . sent)
                        (user-mail-address           . "aaron@hippl.es")
                        (mu4e-compose-signature      . "Aaron"))
                      t)

  (setq mu4e-bookmarks '((:name  "Unread messages"
                          :query "flag:unread AND NOT flag:trashed"
                          :key ?u)
                         (:name "Today's messages"
                          :query "date:today..now"
                          :key ?t)
                         (:name "Last 7 days"
                          :query "date:7d..now"
                          :hide-unread t
                          :key ?w)
                         (:name "Messages with audio"
                          :query "mime:audio/*"
                          :key ?a)))
  )
(when IS-WORK
  (set-email-account! "aaron@ironcladhq.com"
                      '((mu4e-sent-folder            . "/[Gmail].Sent Mail")
                        (mu4e-drafts-folder          . "/[Gmail].Drafts")
                        (mu4e-trash-folder           . "/[Gmail].Trash")
                        (mu4e-refile-folder          . "/[Gmail].All Mail")
                        (smtpmail-smtp-user          . "aaron@ironcladhq.com")
                        (smtpmail-smtp-server        . "smtp.gmail.com")
                        (mu4e-sent-messages-behavior . delete)
                        (user-mail-address           . "aaron@ironcladhq.com")
                        (mu4e-compose-signature      . "Aaron Hipple | Software Engineer\nJoin our Community [https://ironcladapp.com/community]\n\nIronclad [https://ironcladapp.com]\nThe digital contracting platform loved by modern legal teams"))
                      t)

  (setq mu4e-bookmarks '((:name  "Unread messages"
                          :query "maildir:/INBOX AND flag:unread AND NOT flag:trashed AND NOT flag:list"
                          :key ?u)
                         (:name  "Unread list messages"
                          :query "maildir:/INBOX AND flag:unread AND NOT flag:trashed AND flag:list"
                          :key ?l)
                         (:name  "Github"
                          :query "maildir:/INBOX AND from:notifications@github.com AND (date:7d..now OR flag:flagged)"
                          :key ?g)
                         (:name  "Jira"
                          :query "maildir:/INBOX AND from:jira@ironcladapp.atlassian.net AND (date:7d..now OR flag:flagged)"
                          :key ?j)
                         (:name  "Expensify"
                          :query "from:concierge@expensify.com AND date:45d..now"
                          :key ?e)
                         (:name  "Calendar"
                          :query "maildir:/INBOX AND mime:text/calendar"
                          :key ?c)
                         (:name "Alerts"
                          :query "maildir:/INBOX AND (from:mongodb-atlas-alerts@mongodb.com OR from:support@sentry.io OR from:alert@dtdg.co)"
                          :key ?a)
                         (:name "Today's messages"
                          :query "date:today..now"
                          :key ?t)
                         (:name "Last 7 days"
                          :query "date:7d..now"
                          :hide-unread t
                          :key ?w)
                         ))
  )

;;; :app rss
(after! elfeed
  (setq elfeed-search-filter "@1-month-ago +unread"
        ))

;;; :ui doom-dashboard
(setq fancy-splash-image (concat doom-private-dir "splash.png"))
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-footer)

;;; :completion ivy
(after! ivy
  (global-set-key (kbd "TAB") 'ivy-mark))

;;; :lang org
(after! org
  (require 'ox-confluence)

  (setq org-columns-default-format "%60ITEM(Task) %6TODO(State) %10Effort(Estimated){:} %10CLOCKSUM"
        org-global-properties '(("Effort_ALL". "0 0:10 0:20 0:30 1:00 2:00 3:00 4:00 6:00 8:00"))
        org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                                  (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "MEETING")))
        org-todo-keyword-faces (quote (("TODO" :foreground "pink" :weight bold)
                                       ("NEXT" :foreground "cadet blue" :weight bold)
                                       ("DONE" :foreground "sea green" :weight bold)
                                       ("WAITING" :foreground "dark goldenrod" :weight bold)
                                       ("HOLD" :foreground "violet" :weight bold)
                                       ("CANCELLED" :foreground "sea green" :weight bold)
                                       ("MEETING" :foreground "violet" :weight bold)))
        org-use-fast-todo-selection t
        org-todo-state-tags-triggers (quote (("CANCELLED" ("CANCELLED" . t))
                                             ("WAITING" ("WAITING" . t))
                                             ("HOLD" ("WAITING") ("HOLD" . t))
                                             (done ("WAITING") ("HOLD"))
                                             ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))))

        org-log-done 'time

        org-archive-mark-done nil
        org-archive-location "%s_archive::* Archived Tasks"

        org-directory "~/.org/"
        org-agenda-files (cond (IS-WORK (quote ("~/.org" "~/.org-jira" "~/org" "~/org/calendars")))
                               (t (quote ("~/.org" "~/org" "~/org/calendars")))
                               )
        org-refile-targets (quote ((nil :maxlevel . 9)
                                   (org-agenda-files :maxlevel . 9)))

        org-default-notes-file (cond (IS-WORK "~/.org/work.org")
                                     (IS-HOME "~/.org/home.org")
                                     (t "~/.org/refile.org")
                                     )
  )


  (advice-add 'org-agenda-quit :before 'org-save-all-org-buffers)
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-agenda-category-icon-alist '(
                                         ("Work" "~/.icons/work.png" nil nil :ascent center :height 16)
                                         ("Home" "~/.icons/home.png" nil nil :ascent center :height 16)
                                         ("Shopping" "~/.icons/shopping.png" nil nil :ascent center :height 16)
                                         ("Calendar" "~/.icons/calendar.png" nil nil :ascent center :height 16)
                                         ))

  (setq browse-at-remote-prefer-symbolic nil)

  (defun ah/remote-url (original)
    "Browse the current file with `browse-url'."
    (require 'browse-at-remote)
    (let ((url
           (save-window-excursion
             (switch-to-buffer (org-capture-get :original-buffer))
             (browse-at-remote-get-url)
             )))
      (cond ((string= url "Sorry, I'm not sure what to do with this.") original)
            (t url))
      ))

  (setq org-capture-templates
        (quote (("t" "todo" entry (file+headline org-default-notes-file "Refile")
                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                ("n" "note" entry (file+headline org-default-notes-file "Notes")
                 "* %? :NOTE:\n%U\n%(ah/remote-url \"%a\")\n")
                ("g" "groceries" entry (file+headline "~/.org/shopping.org" "Groceries")
                 "* TODO %?\n%U\n")
                ("j" "diary" entry (file+olp+datetree "~/.org/diary.org.gpg")
                 "* %?\n%U\n%a")
                ("m" "meeting" entry (file+headline org-default-notes-file "Meetings")
                 "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                ("h" "habit" entry (file org-default-notes-file)
                 "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

  (setq org-agenda-breadcrumbs-separator " > "
        org-agenda-compact-blocks nil
        org-agenda-dim-blocked-tasks nil
        org-deadline-warning-days 5
        org-hide-emphasis-markers t
        org-agenda-block-separator (string-to-char " ")
   )

  (setq org-agenda-format-date 'my-org-agenda-format-date-aligned)

  (defun my-org-agenda-format-date-aligned (date)
    "Format a DATE string for display in the daily/weekly agenda, or timeline.
This function makes sure that dates are aligned for easy reading."
    (require 'cal-iso)
    (let* ((dayname (calendar-day-name date 1 nil))
           (day (cadr date))
           (day-of-week (calendar-day-of-week date))
           (month (car date))
           (monthname (calendar-month-name month 1))
           (year (nth 2 date))
           (iso-week (org-days-to-iso-week
                      (calendar-absolute-from-gregorian date)))
           (weekyear (cond ((and (= month 1) (>= iso-week 52))
                            (1- year))
                           ((and (= month 12) (<= iso-week 1))
                            (1+ year))
                           (t year)))
           (weekstring (if (= day-of-week 1)
                           (format " W%02d" iso-week)
                         "")))
      (format "%-2s. %2d %s, %s"
              dayname day monthname year)))

  (setq org-agenda-custom-commands
        (quote (("a" "Agenda"
                 ((todo "NEXT" (
                                (org-agenda-overriding-header "\n⚡ NEXTs:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                                (org-agenda-remove-tags t)
                                (org-agenda-prefix-format " %-2i %-15b")
                                (org-agenda-todo-keyword-format "")
                                (org-agenda-todo-list-sublevels nil)
                                (org-agenda-todo-ignore-with-date t)
                                (org-agenda-skip-deadline-if-done t)
                                ))
                  (todo "TODO" (
                                (org-agenda-overriding-header "\n⚡ TODOs:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                                (org-agenda-remove-tags t)
                                (org-agenda-prefix-format " %-2i %-15b")
                                (org-agenda-todo-keyword-format "")
                                (org-agenda-todo-list-sublevels nil)
                                (org-agenda-todo-ignore-with-date t)
                                (org-agenda-skip-deadline-if-done t)
                                ))
                  (agenda ""
                          (
                           (org-agenda-overriding-header "\n⚡ Schedule:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                           (org-agenda-start-day "+0d")
                           (org-agenda-span 5)
                           (org-agenda-remove-tags t)
                           (org-agenda-start-on-weekday nil)
                           (org-agenda-prefix-format " %-3i  %-15b%t %s")
                           (org-agenda-todo-keyword-format "")
                           (org-agenda-scheduled-leaders '("" ""))
                           (org-agenda-skip-scheduled-if-done t)
                           (org-agenda-skip-scheduled-if-deadline-is-shown t)
                           (org-agenda-skip-timestamp-if-done t)
                           (org-agenda-skip-deadline-if-done t)
                           (org-agenda-current-time-string "<┈┈┈┈┈┈┈┈┈┈┈┈ now")
                           (org-agenda-time-grid (quote ((daily today remove-match)
                                                         (0900 1100 1300 1500 1700)
                                                         "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈")))
                 )))
                 ((org-agenda-tag-filter-preset (cond
                                                 (IS-WORK '("-HOME" "-SHOPPING"))
                                                 (IS-HOME '("-WORK"))
                                                 (t '())
                                                 )))
                 )
                ("h" "Start Page"
                 ((todo "NEXT" (
                                (org-agenda-overriding-header "\n⚡ NEXTs:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                                (org-agenda-remove-tags t)
                                (org-agenda-prefix-format " %-2i %-15b")
                                (org-agenda-todo-keyword-format "")
                                (org-agenda-todo-list-sublevels nil)
                                (org-agenda-todo-ignore-with-date t)
                                (org-agenda-skip-deadline-if-done t)
                                ))
                  (todo "TODO" (
                                (org-agenda-overriding-header "\n⚡ TODOs:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                                (org-agenda-remove-tags t)
                                (org-agenda-prefix-format " %-2i %-15b")
                                (org-agenda-todo-keyword-format "")
                                (org-agenda-todo-list-sublevels nil)
                                (org-agenda-todo-ignore-with-date t)
                                (org-agenda-skip-deadline-if-done t)
                                ))
                  (agenda ""
                          (
                           (org-agenda-overriding-header "\n⚡ Schedule:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                           (org-agenda-start-day "+0d")
                           (org-agenda-span 5)
                           (org-agenda-remove-tags t)
                           (org-agenda-start-on-weekday nil)
                           (org-agenda-prefix-format " %-3i  %-15b%t %s")
                           (org-agenda-todo-keyword-format "")
                           (org-agenda-scheduled-leaders '("" ""))
                           (org-agenda-skip-scheduled-if-done t)
                           (org-agenda-skip-scheduled-if-deadline-is-shown t)
                           (org-agenda-skip-timestamp-if-done t)
                           (org-agenda-skip-deadline-if-done t)
                           (org-agenda-show-current-time-in-grid nil)
                           (org-agenda-time-grid (quote ((daily today remove-match)
                                                         (0900 1100 1300 1500 1700)
                                                         "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈")))
                           )))
                 ((org-agenda-with-colors t)
                  (htmlize-output-type 'inline-css)
                  (org-agenda-tag-filter-preset (cond
                                                 (IS-WORK '("-HOME" "-SHOPPING"))
                                                 (IS-HOME '("-WORK"))
                                                 (t '())
                                                 ))
                  ) ("~/.agenda.html"))
                )
               )
        ))

(when IS-WORK
    ;; org-jira
    (after! org-jira
      (setq jiralib-url "https://ironcladapp.atlassian.net")
      (setq org-jira-done-states
            '("Needs Verification" "Closed" "Released" "Verified"))
      (setq org-jira-progress-issue-flow
            '(("To Do" . "In Development")
              ("Needs Verification" . "Verified")
              ))
      (setq org-jira-download-comments t)

      (add-to-list 'jiralib-worklog-import--filters-alist
                   '(t "WorklogAuthoredByCurrentUser"
                       (lambda (wl) (let-alist wl
                                      (when
                                          (and wl
                                               (string-equal
                                                (downcase
                                                 (or jiralib-user-login-name user-login-name))
                                                (downcase \.author\.name)))
                                        wl)))))

      )

    (after! slack
      (slack-register-team
       :name "ironcladinc"
       :default t
       :token (auth-source-pick-first-password
               :host "ironcladinc.slack.com"
               :user "aaron@ironcladhq.com")
       :subscribed-channels '(team-eng team-hermes-eng team-product team-hermes)
       :full-and-display-names t))
    )

(use-package! alert
  :init
  (when IS-WORK
    (setq alert-default-style 'notifier))
  )
